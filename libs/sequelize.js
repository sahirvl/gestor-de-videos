
const { Sequelize } = require('sequelize');
const { config } = require('../config/config');
const setupModels = require('../db/models');

const sequelize = new Sequelize({
  database: config.dbName,
  username: config.dbUser,
  password: config.dbPassword,
  host: config.dbHost,
  dialect: 'mysql'
});

sequelize
  .authenticate()
  .then(() => console.log('Connection has been established successfully.'))
  .catch((error) => console.error('Unable to connect to the database:', error));

setupModels(sequelize)

module.exports = sequelize;
