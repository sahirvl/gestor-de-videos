const Joi = require('joi');

const id = Joi.number().integer();
const title = Joi.string().min(5).max(30)
const description = Joi.string().min(4).max(100)
const authorId = Joi.number().integer();
const createAt = Joi.date();

const createSchema = Joi.object({
  title: title.required(),
  description: description.required(),
  authorId: authorId.required(),
});

const updateSchema = Joi.object({
  title,
  description,
});

const getSchema = Joi.object({
  id,
  authorId,
});

module.exports = { createSchema, updateSchema, getSchema };
