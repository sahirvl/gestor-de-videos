const Joi = require('joi');

const id = Joi.number().integer();
const name = Joi.string().min(1).max(15)
const lastName = Joi.string().min(1).max(15)
const password = Joi.string().min(6).max(15);
const email = Joi.string().email();
const role = Joi.string();
const createAt = Joi.date();

const createSchema = Joi.object({
  name: name.required(),
  lastName: lastName.required(),
  password: password.required(),
  email: email.required(),
  role: role.required(),
});

const updateSchema = Joi.object({
  name: name,
  lastName: lastName,
  password: password,
  email: email,
  role: role,
});

const getSchema = Joi.object({
  id: id,
  email: email,
  role: role,
});

module.exports = { createSchema, updateSchema, getSchema };
