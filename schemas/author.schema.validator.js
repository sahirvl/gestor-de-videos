const Joi = require('joi');

const id = Joi.number().integer();
const userId = Joi.number().integer()
const createAt = Joi.date();

const createSchema = Joi.object({
  userId: userId.required()
});

const getSchema = Joi.object({
  id: id,
  userId,
});

module.exports = { createSchema, getSchema };
