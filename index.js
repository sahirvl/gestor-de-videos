const express = require('express');
const passport = require('passport')

//const cors = require('cors');
const routerApi = require('./routes');


const {
  logErrors,
  errorHandler,
  boomErrorHandler,
  ormErrorHandler,
} = require('./middlewares/error.handler');
const { config } = require('./config/config');

const app = express();
const port = config.port;

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

/*const whitelist = [''];
const options = {
  origin: (origin, callback) => {
    if (whitelist.includes(origin) || !origin) {
      callback(null, true);
    } else {
      callback(new Error('no permitido'));
    }
  },
};
app.use(cors(options));*/

require('./utils/auth')
//app.use(passport.initialize());


app.get('/', (req, res) => {
  res.send('MY SERVER RETO');
});

routerApi(app);

app.use(logErrors);
app.use(ormErrorHandler);
app.use(boomErrorHandler);
app.use(errorHandler)

app.listen(port, () => {
  console.log('++++++ !HAPPY HACKING¡ ++++++');
});
