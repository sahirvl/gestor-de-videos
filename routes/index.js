const express = require('express');

const usersRouter = require('./user.route');
const authorRouter = require('./author.route');
const videoRouter = require('./video.route');
const authRouter = require('./auth.route');
const profileRouter = require('./profile.route')

function routerApi(app) {
  const router = express.Router();
  app.use('/api', router);
  router.use('/user', usersRouter);
  router.use('/author', authorRouter);
  router.use('/video', videoRouter);
  router.use('/auth', authRouter);
  router.use('/profile', profileRouter)
}

module.exports = routerApi;
