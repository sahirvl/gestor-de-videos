const express = require('express');
const passport = require('passport')
const validatorHandler = require('./../middlewares/validator.handler')
const { getSchema, createSchema, updateSchema } = require('../schemas/user.schema.validator');
const UserService = require('./../services/user.service');

const router = express.Router();
const service = new UserService();

router.get('/', async (req, res, next) => {
  try {
    const users = await service.find();
    res.status(200).json(users);
  } catch (error) {
    next(error);
  }
});

router.get('/search',
  validatorHandler(getSchema, 'query'),
  async (req, res, next) => {
    try {
      const { id } = req.query;
      const user = await service.findOne(id);
      res.status(200).json(user);
    } catch (error) {
      next(error);
    }
  }
);

router.get('/email',
  validatorHandler(getSchema, 'query'),
  async (req, res, next) => {
    try {
      const { email } = req.query;
      const user = await service.findEmail(email);
      res.status(200).json(user);
    } catch (error) {
      next(error);
    }
  }
);

router.post('/',
  validatorHandler(createSchema, 'body'),
  async (req, res, next) => {
    try {
      const body = req.body;
      const newUser = await service.create(body);
      res.status(201).json(newUser);
    } catch (error) {
      next(error);
    }
  }
);

router.get('/author-id',
  passport.authenticate('jwt', { session: false }),
  async (req, res, next) => {
    try {
      const user = req.user;
      console.log(user)
      const authorId = await service.findAuthor(user.sub)
      console.log(authorId)
      res.json(authorId);
    } catch (error) {
      next(error);
    }
  }
);

router.patch('/updt',
  validatorHandler(getSchema, 'query'),
  validatorHandler(updateSchema, 'body'),
  async (req, res, next) => {
    try {
      const { id } = req.query;
      const body = req.body;
      const updtUser = await service.update(id,body);
      res.status(201).json({message: 'Updated',updtUser});
    } catch (error) {
      next(error);
    }
  }
);

router.delete('/del',
validatorHandler(getSchema, 'query'),
async (req, res, next) => {
  try {
    const { id } = req.query;
    const user = await service.findOne(id)
    if (id) {
      await service.delete(id);
      res.status(201).json({message: 'deleted', user});
    }

  } catch (error) {
    next(error);
  }
}
);

module.exports = router;

