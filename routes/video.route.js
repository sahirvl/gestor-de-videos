const express = require('express');
const validatorHandler = require('./../middlewares/validator.handler')
const { createSchema } = require('../schemas/video.schema.validator');
const VideoService = require('../services/video.service');
const passport = require('passport');

const router = express.Router();
const service = new VideoService();

router.get('/', async (req, res, next) => {
  try {
    const video = await service.find();
    res.status(200).json(video);
  } catch (error) {
    next(error);
  }
});

router.post('/',
passport.authenticate('jwt', {session: false}),
validatorHandler(createSchema, 'body'),
async (req, res, next) => {
  try {
    const body = req.body;
    const video = await service.create(body);
    res.status(201).json(video);
  } catch (error) {
    next(error);
  }
});


module.exports = router;
