const express = require('express');
const passport = require('passport')
const validatorHandler = require('./../middlewares/validator.handler')
const { getSchema, createSchema } = require('../schemas/author.schema.validator');
const AuthorService = require('../services/author.service');

const router = express.Router();
const service = new AuthorService

router.get('/', async (req, res, next) => {
  try {
    const authors = await service.find();
    res.status(200).json(authors);
  } catch (error) {
    next(error);
  }
});

router.post('/',
validatorHandler(createSchema, 'body'),
  async (req, res, next) => {
    try {
      const body = req.body;
      const author = await service.create(body);
      res.status(201).json(author);
    } catch (error) {
      next(error);
    }
  }
);

router.get('/user-id',
  passport.authenticate('jwt', { session: false }),
  async (req, res, next) => {
    try {
      const user = req.user;
      console.log(user)
      const userId = await service.findUserByPk(user.sub)
      console.log(userId)
      res.json(userId);
    } catch (error) {
      next(error);
    }
  }
);

module.exports = router;

