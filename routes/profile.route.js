const express = require('express');
const passport = require('passport');
const VideoService = require('../services/video.service');


const router = express.Router();
const videoService = new VideoService


router.get('/my-videos',
  passport.authenticate('jwt', { session: false }),
  async (req, res, next) => {
    try {
      const user = req.user;
      console.log(user)
      const video = await videoService.findByAuthor(user.sub)
      res.json(video);
    } catch (error) {
      next(error);
    }
  }
);

module.exports = router
