const { config } = require('../config/config')



module.exports = {
  development: {
    "username": config.dbUser,
    "password": config.dbPassword,
    "database": config.dbName,
    "host": config.dbHost,
    "dialect": "mysql"
  },
  "test": {
    "username": encodeURIComponent(config.dbUser),
    "password": encodeURIComponent(config.dbPassword),
    "database": encodeURIComponent(config.dbName),
    "host": encodeURIComponent(config.dbHost),
    "dialect": "mysql"
  },
  "production": {
    "username": encodeURIComponent(config.dbUser),
    "password": encodeURIComponent(config.dbPassword),
    "database": encodeURIComponent(config.dbName),
    "host": encodeURIComponent(config.dbHost),
    "dialect": "mysql"
  }
}
