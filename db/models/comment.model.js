const { Model, DataTypes, Sequelize } = require('sequelize');

const { USER_TABLE } = require('./user.model');
const { VIDEO_TABLE } = require('./video.model');

const COMMENT_TABLE = 'commentaries';

const CommentSchema = {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: DataTypes.INTEGER,
  },
  text: {
    allowNull: false,
    type: DataTypes.TEXT,
  },
  userId: {
    field: 'user_id',
    allowNull: true,
    type: DataTypes.INTEGER,
    references: {
      model: USER_TABLE,
      key: 'id',
    },
    onUpdate: 'CASCADE',
    onDelete: 'SET NULL',
  },
  videoId: {
    field: 'video_id',
    allowNull: true,
    type: DataTypes.INTEGER,
    references: {
      model: VIDEO_TABLE,
      key: 'id',
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE,
      field: 'created_at',
      defaultValue: Sequelize.NOW,
    },
  },
};

class Commentaries extends Model {
  static associate(models) {
    // associate
  }

  static config(sequelize) {
    return {
      sequelize,
      tableName: 'commentaries',
      modelName: 'Commentaries',
      timestamps: false,
    };
  }
}

module.exports = { COMMENT_TABLE, CommentSchema, Commentaries };
