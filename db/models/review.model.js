const { Model, DataTypes, Sequelize } = require('sequelize');

const { USER_TABLE } = require('./user.model');
const { VIDEO_TABLE } = require('./video.model');

const REVIEW_TABLE = 'reviews';

const ReviewSchema =  {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: DataTypes.INTEGER
  },
  score: {
    allowNull: false,
    type: DataTypes.INTEGER
  },
  text: {
    allowNull: false,
    type: DataTypes.TEXT
  },
  userId: {
    field: 'user_id',
    allowNull: true,
    type: DataTypes.INTEGER,
    references: {
      model: USER_TABLE,
      key: 'id'
    },
    onUpdate: 'CASCADE',
    onDelete: 'SET NULL'
  },
  videoId: {
    field: 'video_id',
    allowNull: true,
    type: DataTypes.INTEGER,
    references: {
      model: VIDEO_TABLE,
      key: 'id'
    },
    onUpdate: 'CASCADE',
    onDelete: 'SET NULL'
  },
  createdAt: {
    allowNull: false,
    type: DataTypes.DATE,
    field: 'created_at',
    defaultValue: Sequelize.NOW,
  },
}

class Review extends Model {

  static associate(models) {
    // associate
  }

  static config(sequelize) {
    return {
      sequelize,
      tableName: 'reviews',
      modelName: 'Review',
      timestamps: false
    }
  }
}

module.exports = { REVIEW_TABLE, ReviewSchema, Review };
