
const { User, UserSchema } = require('./user.model');
const { Author, AuthorSchema } = require('./author.model');
const { Collaborator, CollabSchema } = require('./collab.model');
const { Video, VideoSchema } = require('./video.model');
const { Commentaries, CommentSchema } = require('./comment.model');
const { Review, ReviewSchema } = require('./review.model');


function setupModels(sequelize) {
  User.init(UserSchema, User.config(sequelize))
  Author.init(AuthorSchema, Author.config(sequelize))
  Collaborator.init(CollabSchema, Collaborator.config(sequelize))
  Video.init(VideoSchema, Video.config(sequelize))
  Commentaries.init(CommentSchema, Commentaries.config(sequelize))
  Review.init(ReviewSchema, Review.config(sequelize))

  User.associate(sequelize.models)
  Author.associate(sequelize.models)
  Video.associate(sequelize.models)
}

module.exports = setupModels
