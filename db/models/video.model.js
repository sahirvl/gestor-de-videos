const { Model, DataTypes, Sequelize } = require('sequelize');
const { AUTHOR_TABLE } = require('./author.model');

const VIDEO_TABLE = 'video';
const VideoSchema = {
  id: {
    allowNull: false,
    autoIncrement: true,
    unique: true,
    primaryKey: true,
    type: DataTypes.INTEGER,
  },
  title: {
    allowNull: false,
    type: DataTypes.STRING,
  },
  description: {
    allowNull: true,
    type: DataTypes.TEXT,
  },
  authorId:{
    field: 'author_id',
    allowNull: true,
    type: DataTypes.INTEGER,
    references: {
      model: AUTHOR_TABLE,
      key: 'id',
    },
    onUpdate: 'CASCADE',
    onDelete: 'SET NULL',
  },
  createAt: {
    allowNull: false,
    type: DataTypes.DATE,
    field: 'create_at',
    defaultValue: Sequelize.NOW,
  }
};

class Video extends Model {
  static associate(models) {
    this.belongsTo(models.Author, {
      as: 'author',
    })
  }

  static config(sequelize) {
    return {
      sequelize,
      tableName: VIDEO_TABLE,
      modelName: 'Video',
      timestamps: false,
    };
  }
}



module.exports = { VIDEO_TABLE, VideoSchema, Video };
