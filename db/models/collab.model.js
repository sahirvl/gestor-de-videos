const { Model, DataTypes, Sequelize } = require('sequelize');
const { USER_TABLE } = require('./user.model')

const COLLAB_TABLE = 'collaborator';
const CollabSchema = {
  id: {
    allowNull: false,
    autoIncrement: true,
    unique: true,
    primaryKey: true,
    type: DataTypes.INTEGER,
  },
  userId: {
    field: 'user_id',
    allowNull: true,
    type: DataTypes.INTEGER,
    unique: true,
    references: {
      model: USER_TABLE,
      key: 'id',
    },
    onUpdate: 'CASCADE',
    onDelete: 'SET NULL',
  },
  videoId: {
    field: 'video_id',
    allowNull: true,
    type: DataTypes.INTEGER,
    References: {
      model: 'video',
      key: 'id',
    },
    onUpdate: 'CASCADE',
    onDelete: 'SET NULL',
  },
  createAt: {
    field: 'create_at',
    allowNull: false,
    type: DataTypes.DATE,
    defaultValue: Sequelize.NOW,
  },
};

class Collaborator extends Model {
  static associate(models) {
    // associate
  }
  static config(sequelize) {
    return {
      sequelize,
      tableName: COLLAB_TABLE,
      modelName: 'Collaborator',
      timestamps: false,
    };
  }
}

module.exports = { COLLAB_TABLE, CollabSchema, Collaborator };
