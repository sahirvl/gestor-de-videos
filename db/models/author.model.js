const { Model, DataTypes, Sequelize } = require('sequelize');
const { USER_TABLE } = require('./user.model')

const AUTHOR_TABLE = 'author';
const AuthorSchema = {
  id: {
    primaryKey: true,
    allowNull: false,
    autoIncrement: true,
    unique: true,
    type: DataTypes.INTEGER,
  },
  userId: {
    fiel: 'user_id',
    unique: true,
    allowNull: true,
    type: DataTypes.INTEGER,
    references: {
      model: USER_TABLE,
      key: 'id',
    },
    onUpdate: 'CASCADE',
    onDelete: 'SET NULL',
  },
  createAt: {
    field: 'create_at',
    allowNull: false,
    type: DataTypes.DATE,
    defaultValue: Sequelize.NOW,
  },
};

class Author extends Model {
  static associate(models) {
    this.belongsTo(models.User, {as: 'user'})
  }
  static config(sequelize) {
    return {
      sequelize,
      tableName: AUTHOR_TABLE,
      modelName: 'Author',
      timestamps: false,
    };
  }
}

module.exports = { AUTHOR_TABLE, AuthorSchema, Author };
