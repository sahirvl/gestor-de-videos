const { UserSchema, USER_TABLE } = require('../models/user.model');
const { AuthorSchema, AUTHOR_TABLE } = require('../models/author.model');
const { CollabSchema, COLLAB_TABLE } = require('../models/collab.model');
const { VideoSchema, VIDEO_TABLE } = require('../models/video.model');
const { CommentSchema, COMMENT_TABLE } = require('../models/comment.model');
const { ReviewSchema, REVIEW_TABLE } = require('../models/review.model');

module.exports = {
  up: async (queryInterface) => {
    await queryInterface.createTable(USER_TABLE, UserSchema);
    await queryInterface.createTable(AUTHOR_TABLE, AuthorSchema);
    await queryInterface.createTable(COLLAB_TABLE, CollabSchema);
    await queryInterface.createTable(VIDEO_TABLE, VideoSchema);
    await queryInterface.createTable(COMMENT_TABLE, CommentSchema);
    await queryInterface.createTable(REVIEW_TABLE, ReviewSchema);
  },

  down: async (queryInterface) => {
    await queryInterface.dropTable(USER_TABLE);
    await queryInterface.dropTable(AUTHOR_TABLE);
    await queryInterface.dropTable(COLLAB_TABLE);
    await queryInterface.dropTable(VIDEO_TABLE);
    await queryInterface.dropTable(COMMENT_TABLE);
    await queryInterface.dropTable(REVIEW_TABLE);
  },
};
