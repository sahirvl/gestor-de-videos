const { models } = require('../libs/sequelize');
const { notFound } = require('boom');

class AuthorService {
  constructor() {

  }
  async create(userId) {
    const author = await models.Author.create(userId);
    delete author.dataValues.password;
    return author;
  }

  async find() {
    const author = await models.Author.findAll({
      include: [{
        association: 'user',
        attributes: [ 'name','lastName','role' ]
      }]    });
    if (author == false) {
      return { message: 'no se hallaron datos' };
    }
    return author;
  }

  async findUserByPk(userId) {
    const user = await models.Author.findOne({
      where: {userId}
    })
    if (!user) {
      throw notFound('no se hallaron datos')
    }
    return user
  }
}

module.exports = AuthorService
