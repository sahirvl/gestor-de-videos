const boom = require('boom');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const UserService = require('./user.service');

const { config } = require('../config/config');
const { unauthorized } = require('boom');


const service = new UserService();

class AuthService {
  async getUser(email, password) {
    const user = await service.findEmail(email);
    if (!user) {
      throw boom.unauthorized();
    }
    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
      throw boom.unauthorized();
    }
    delete user.dataValues.password;
    return user;
  }

  signToken(user) {
    const payload = {
      sub: user.id,
      email: user.email,
      role: user.role,
    };
    const token = jwt.sign(payload, config.jwtSecret);
    return {
      user: payload,
      token,
    };
  }

  async sendMail(infoMail) {
    const transporter = nodemailer.createTransport({
      host: 'smtp.gmail.com',
      secure: true, // true for 465, false for other ports
      port: 465,
      auth: {
        user: config.mailerEmail,
        pass: config.mailerPassword,
      },
    });

    await transporter.sendMail(infoMail);
    return { message: 'Mail sent' };
  }

  async sendRecovery(email) {
    const user = await service.findEmail(email);
    if (!user) {
      throw boom.unauthorized();
    }
    const payload = {sub: user.id}
    const token = jwt.sign(payload, config.jwtRecoverySecret,{expiresIn: '15min'});
    const link = `https://myfrontend.com/recovery?token=${token}`
    await service.update(user.id, {recoveryToken: token})
    const mail = {
      from: config.mailerEmail, // sender address
      to: `${user.email}`, // list of receivers
      subject: 'Nuevo correo de prueba', // Subject line
      text: 'Estoy usando Nodemailer!', // plain text body
      html: `<b>Ingresa a el siguiente link ${link}</b>`, // html body
  }
  const rta = await this.sendMail(mail)
  return rta
}

async changePassword(token, newPassword) {
  try {
    const payload = jwt.verify(token, config.jwtRecoverySecret)
    const user = await service.findOne(payload.sub)
    if (user.recoveryToken !== token) {
      throw unauthorized()
    }
    const hash = await bcrypt.hash(newPassword, 10)
    await service.update(user.id, { recoveryToken: null, password: hash })
    return {massage: 'password changed'}
  } catch (error) {
    throw unauthorized()
  }
}
}

module.exports = AuthService;
