const { models } = require('../libs/sequelize');
const { notFound } = require('boom');
const { Op } = require('sequelize');

class UserService {
  constructor() {

  }
  async create(data) {
    const newUser = await models.User.create(data);
    delete newUser.dataValues.password;
    return newUser;
  }

  async find() {
    const user = await models.User.findAll({
      order: [['name', 'ASC']],
      attributes: {
        exclude: ['password'],
      },
    });
    if (!user || user == false) {
      throw notFound()
    }
    return user;
  }

  async findOne(id) {
    const user = await models.User.findAll({
      where: {
        id
      },
    });
    if (!user || user == false) {
      throw notFound()
    }
    return user;
  }

  async findEmail(email) {
    const user = await models.User.findOne({
      where: { email },
      attributes: {
        exclude: ['createAt']
    },
    include: ['author']
  });
    if (!user || user == false) {
      throw notFound()
    }
    return user;
  }

  async findAuthorId(id) {
    const user = await models.User.findOne({
      where: { id },
      include: ['author']
    })
    if (!user) {
      throw notFound('no se hallaron datos')
    }
    return user.author.id
  }

  async update(id, changes) {
    const user = await this.findOne(id);
    const rta = await user.update(changes);
    return rta;
  }

  async delete(id) {
    const user = await this.findOne(id);
    if (!user || user == false) {
      throw notFound()
    }
    await user.destroy();
    return { id };
  }
}

module.exports = UserService;
