const { models } = require('../libs/sequelize');
const { notFound } = require('boom');

class VideoService {
  constructor() {}
  async create(data) {
    const video = await models.Video.create(data);
    return video;
  }

  async find() {
    const video = await models.Video.findAll({
      include: [
        {
          association: 'author',
          attributes: ['userId'],
          include: [
            {
              association: 'user',
              attributes: ['name', 'lastName'],
            },
          ],
        },
      ],
    });
    if (video == false) {
      return { message: 'no se hallaron datos' };
    }
    return video;
  }

  async findByAuthor(id) {
    // id del user de entrada es 6 el cual va en el sub del token
    const user = await models.User.findOne({
      where: { id }, // en donde id = 6
      attributes: ['id'], // solo el atributo id
      include: ['author'], // incluyendo la relacion del userid6 con author
    });
    if (!user) {
      // si no existe el usuario 6
      throw notFound('no se halaron datos'); // lance el not found
    }
    const authorId = user.author.id; // authorId es = el user 6 se relaciona con el author  id = 4
    const video = await models.Video.findAll({
      // busque todos los videos
      where: { authorId }, // donde encuentre que el authorId sea = 4
    });
    return video; // los videos del author 4
  }
}

module.exports = VideoService;
